//
//  openmapApp.swift
//  openmap
//
//  Created by Francisco Perez on 16/05/23.
//

import SwiftUI

@main
struct openmapApp: App {
    var body: some Scene {
        WindowGroup {
            
            ContentView() 
            //mapView()
        }
    }
}
