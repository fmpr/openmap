//
//  welcomeView.swift
//  openmap
//
//  Created by Francisco Perez on 16/05/23.
//

import Foundation
import SwiftUI
import CoreLocationUI

struct welcomeView: View {
    
    @EnvironmentObject var locationManager: LocationManager

        var body: some View {
            VStack {
                VStack(spacing: 20) {
                    Text("Welcome to your weather App")
                        
                        .font(.title)
                    
                    Text("Please share your current location")
                        .padding()
                }
                .multilineTextAlignment(.center)
                .padding()

                
                // LocationButton from CoreLocationUI framework imported above, allows us to requestionLocation
                LocationButton(.shareMyCurrentLocation) {
                    locationManager.requestLocation()
                }
                .cornerRadius(30)
                .symbolVariant(.fill)
                .foregroundColor(.white)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
}

struct welcomeView_Previews: PreviewProvider {
    static var previews: some View {
        welcomeView()
    }
}
