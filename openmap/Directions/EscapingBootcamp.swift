//
//  EscapingBootcamp.swift
//  openmap
//
//  Created by Francisco Perez on 06/06/23.
//

import SwiftUI

class EscapingViewModel:ObservableObject {
    @Published var text: String = "Hello"
    
    func getData() {
        //let newData = dowloaddata()
        //text = newData
        
        
        
//        downloadData3 { [weak self](returnedData) in
//            self?.text = returnedData
//        }
        
        downloadData4 { [weak self] (returnedResult) in
            self?.text = returnedResult.data
        }
    }
    
    func dowloaddata() -> String {
        return "new data!"
    }
    
    // 1.-
    func downloadData2(completionhandler: (_ data:String)-> Void) {
        completionhandler("New data here")
    }
    
    // 1.- the same,but completionHandler
    func doSomething(data:String) {
        print(data)
    }
    
    func downloadData3(completionhandler: @escaping (_ data:String)-> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            completionhandler("New data here")
        }
    }
    
    
    func downloadData4(completionhandler: @escaping DownloadCompletion) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            let result = DowloadResult(data:"New data from struct")
            completionhandler(result)
        }
    }
    
    
    
} // end view model

struct DowloadResult {
    let data: String
}

typealias DownloadCompletion = (DowloadResult) -> Void


struct EscapingBootcamp: View {
    
    @StateObject var vm = EscapingViewModel()
    var body: some View {
        Text(vm.text)
            .font(.largeTitle)
            .foregroundColor(.blue)
            .onTapGesture {
                vm.getData()
            }
    }
}

struct EscapingBootcamp_Previews: PreviewProvider {
    static var previews: some View {
        EscapingBootcamp()
    }
}
