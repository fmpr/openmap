//
//  ContentView.swift
//  openmap
//
//  Created by Francisco Perez on 16/05/23.
//


/*
Description: we use the class Managers/LocationManager for our current location
*/


import SwiftUI

struct ContentView: View {
    
        @StateObject var locationManager = LocationManager()
        var weatherManager = WeatherManager()
        @State var weather: ResponseBody?
        
        var body: some View {
            VStack {
                if let location = locationManager.location {
                    if let weather = weather {
                        //WeatherView(weather: weather)
                        mapView(name: weather.name)
                        
                    } else {
                        LoadingView()
                            .task {
                                do {
                                    weather = try await weatherManager.getCurrentWeather(latitude: location.latitude, longitude: location.longitude)
                                } catch {
                                    print("Error: \(error)")
                                }
                            }
                    }
                } else {
                    if locationManager.isLoading {
                        LoadingView()
                    } else {
                        welcomeView()
                            .environmentObject(locationManager)
                    }
                }
            }
            .background(Color(hue: 0.656, saturation: 0.787, brightness: 0.354))
            .preferredColorScheme(.dark)
     
       }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
