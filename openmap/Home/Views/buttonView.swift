


import Foundation
import SwiftUI


struct buttonView:View {
    
    var title: String
    var textColor: Color
    var backgroundColor: Color
    var body: some View{
        
        Text(title)
            .frame(width: 100, height: 50, alignment: .center)
            .background(backgroundColor)
            .foregroundColor(textColor)
            .font(.system(size:20,weight: .bold,design: .default))
            .cornerRadius(10)
    }
}
