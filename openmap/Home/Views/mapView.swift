//
//  mapView.swift
//  openmap
//
//  Created by Francisco Martin on 16/05/23.
//

import SwiftUI

struct mapView: View {
    
    let name:String
    
    
    @State private var isNight = false
    @StateObject var weatherViewModel = weatherVM()
    
    @State private var location: String = ""
    @State private var showingAlert = false
    var body: some View {

            
            
            ZStack {
                BackgroudView(isNight: $isNight,topcolor: isNight ? .black : .blue, bottomcolor: isNight ? .gray :  Color("lightblue"))
                
                VStack{
                    
                    let arreglo:Array<Weather> = [Weather(id: 0, main: "",description: "",icon: "")]
                    
                    let temp:Double = weatherViewModel.weather_model.main?.temp ?? 0
                    let des:[Weather] = weatherViewModel.weather_model.weather ?? arreglo
                    let humidity = weatherViewModel.weather_model.main?.humidity ?? 0
                    let pressure = weatherViewModel.weather_model.main?.pressure ?? 0
                    
                    //here
                    CityTextView(cityName: weatherViewModel.weather_model.name ?? "\(name)")
                    
                    VStack(spacing: 10){
                        //here
                        MainWheaterStatusView(imageName: isNight ? "moon.stars.fill" : "cloud.sun.fill", tempareature: temp, description: des[0].description.lowercased(),icon: des[0].icon)
                        
                    }
                    .padding(.bottom,40)
                    
                    HStack(spacing: 20){
                        WheaterdayView(dayoftheweek: "Humidity", imageName: "cloud.sun.fill", temparature: humidity)
                        WheaterdayView(dayoftheweek: "Pressure", imageName: "sun.max.fill", temparature: pressure)
                        
                    }
                    .padding(.bottom,50)
                    
                    
                    HStack(){
                        TextField("Enter a US city ",text: $location)
                            .frame(width: 250, height: 100, alignment: .center)
                            .font(.system(size:25,weight: .bold,design: .default))
                            .cornerRadius(10)
                            .ignoresSafeArea()
                            .foregroundColor(.white)
                            .textFieldStyle(.roundedBorder)
                        
 
                        
                        Button{
                            isNight.toggle()
                            showingAlert = false
                            // weatherViewModel.fetch()
                            Task {
                                //await weatherViewModel.getCityInfo(city:location)
                                
                                do {
                                    try await weatherViewModel.getAPIData(city: location)
                                } catch let error {
                                    showingAlert = true
                                    print(error.localizedDescription)
                                }
                            }
                            
                            
                        }
                        
                        label: {
                            buttonView(title: "Search", textColor: Color.blue, backgroundColor: .white)
                        }
                        
                    }
                    
                    Button{
                        UserDefaults.standard.removeObject(forKey: "lastlocationUD")
                    }
                    label : {
                        VStack {
                            
                            Image(systemName: "star.fill")
                            Text("Clear my last location")
                            
                        }
                    }
                    
                    .onAppear{
                        
                        let lastlocationUD:String = UserDefaults.standard.object(forKey: "lastlocationUD") as? String ?? name
                        
                        Task {
                            do {
                                try await weatherViewModel.getAPIData(city: lastlocationUD)
                            } catch let error {
                                print(error.localizedDescription)
                            }
                            
                            // it works with DispatchQueue
                            //await weatherViewModel.getCityInfo(city:lastlocationUD)
                            
                        }
                    }
                }
                
            }
        
        
    }// end body
        
        
        
}// end view

struct Weather_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            mapView(name: "local")
        }
    }
}

struct WheaterdayView: View {
    
    var dayoftheweek: String
    var imageName: String
    var temparature: Int
    
    var body: some View {
        
        VStack {
            Text(dayoftheweek)
                .font(.system(size:16,weight: .medium, design: .default))
                .foregroundColor(Color.white)
            
        
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width:40, height:40)
             
            
            Text("\(temparature)")
                .font(.system(size:20,weight: .light))
                .foregroundColor(Color.white)
        }
    }
}

struct BackgroudView: View {
    @Binding var isNight: Bool
    var topcolor: Color
    var bottomcolor: Color
    
    var body: some View {
        
        LinearGradient(colors: [isNight ? .black: .blue,isNight ? .gray : Color(.lightGray)], startPoint: .topLeading, endPoint: .bottomTrailing)
            .edgesIgnoringSafeArea(.all)
    }
}

struct CityTextView: View {
    var cityName: String
    
    var body: some View{
        Text(cityName)
            .font(.system(size:32,weight: .medium,design: .default))
            .foregroundColor(Color.white)
            .padding()
    }
} // end_CityTextView


struct MainWheaterStatusView: View{
    
    var imageName: String
    var tempareature: Double
    var description:String
    var icon:String
    

    var body: some View{
        
        
   
        let faren_temp:Int = Int((tempareature * 9/5) - 459.67)
        
        
        // i am using asyncimage for loading a url image
        AsyncImage(url: URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png"))
            .frame(width:40, height:40)
        
        /*Image(systemName: imageName)
            .renderingMode(.original)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width:170, height:170)
         */
        
        Text("\(faren_temp) °F")
            .font(.system(size:70,weight: .medium))
            .foregroundColor(Color.white)
        
        Text(description)
            .font(.system(size:50,weight: .medium))
            .foregroundColor(Color.white)
    }
} // end_MainWheaterStatusView
