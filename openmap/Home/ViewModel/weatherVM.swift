
/*
Created by Francisco Perez on 16/05/23.
Description:
 - fetchdata is a dummy function
 - fetchdata2 we fetching the data from api.openweathermap.org using our key and city name
 - i used POSTMAN for urlstring
 - using our appid key in code its ok but when we use reverse reverse engineering we expose all the keys
*/



import Foundation
import UIKit
import Combine


class weatherVM: ObservableObject {
    
    // class that conform NetworkInterface (protocol)
    // Dependency Injection
    // func fetchData(url:URL) async throws -> Data
    
    private var networkSession: NetworkInterface
    var isFetchCalled = false
    var respondeDataBytes: Int = 0
   
    
    //here the model
    @Published var weather_model = weatherModel()
    
    init(networkSession: NetworkInterface = NetworkSession()) {
        self.networkSession = networkSession
    }

    
//    func getPost() {
//
//
//        // this works woth combine
//        guard let urlString = URL(string: "example") else { return }
//        
//        // 1.- create the publisher
//        // 2.- susbcribe publisher on backgrouund thread
//        // 3.- receive on Main Thread
//        //4.- tryMap
//        
//        URLSession.shared.dataTaskPublisher(for: urlString)
//            .subscribe(on: DispatchQueue.global(qos: .background))
//            .receive(on: DispatchQueue.main)
//            .tryMap { (data, response) -> Data in
//                guard let response = response as? HTTPURLResponse,
//                      response.statusCode >= 200 && response.statusCode < 300 else {
//                    throw URLError(.badServerResponse)
//                }
//                return data
//            } // end trymap
//            .decode(type: weather_model.self, decoder: JSONDecoder())
//            .sink { (completion) in
//                print(" \(completion)")
//            } receiveValue: { [weak self](returnedPosts) in
//                self?.weather_model
//            }
//            
//        
//    }
    
    // ********************************************************************************************************************
    //  example with ACTOR  @mainActor
    //  func getCityInfo(city:String) async
    //  Clean code
    //  I am using a global ACTOR instead DispatchQueue.main to update the UI on the main thread
    //  A global actor providing an executor which performs its tasks on the main thread,
    //  it provides a safe and efficient way to manage shared mutable state
    // ********************************************************************************************************************
    
    
    // just US cities
    
    @MainActor
    func getAPIData(city:String) async throws {
        var urlstring : String
        urlstring = "https://api.openweathermap.org/data/2.5/weather?q=\(city),US&appid=97fb0baad55faa308a3ff5ccc0a2d19e"
        
        guard let url = URL(string: urlstring)
        else {
             respondeDataBytes = 0
             throw URLError(.badURL)
            
        }
        // save the last city
        UserDefaults.standard.set(city, forKey: "lastlocationUD")
        let responseData = try await networkSession.fetchData(url: url)
        respondeDataBytes = 1
        isFetchCalled = true
        
        let infoData = try JSONDecoder().decode(weatherModel.self, from: responseData)
        weather_model = infoData
    }
    
    // *********************************************************************************************************************
    // example with GCD
    //  func getCityInfo(city:String) async
    // it works with DispatchQueue and it updates the UI on the main thread
    // **********************************************************************************************************************
    
    
    func getCityInfo(city:String) async {
        
        // i am using async and await feature and GCD
        
        var urlstring : String
        urlstring = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=97fb0baad55faa308a3ff5ccc0a2d19e"
        
        
        UserDefaults.standard.removeObject(forKey: "lastlocationUD")
        
        
        guard let url = URL(string: urlstring)
        else { print("error in json EndPoint")
            return}
        
        do {
            
            // URLsession for fetching de information
            let (data,_) = try await URLSession.shared.data(from: url)
            
            if let decodedResponse = try? JSONDecoder().decode(weatherModel.self, from: data){
                
                // ******************************************************************************************************
                // we need to display the info on the main thread
                // ******************************************************************************************************
                
                DispatchQueue.main.async {
                    self.weather_model = decodedResponse
                    UserDefaults.standard.set(city, forKey: "lastlocationUD")
                }
            }
            else
            {
                print("Error in decoded Response check the Model.swift")
            }
        }
        catch let jsonError as NSError {
            print("JSON decode failed: \(jsonError.localizedDescription)")
            
        }
    } // end_async
    
}
