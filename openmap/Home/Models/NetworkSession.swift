//
//  NetworkInterface.swift
//  openmap
//
//  Created by Francisco Perez on 22/05/23.
//

import Foundation

class NetworkSession: NetworkInterface {
    
    
    private var urlSession: URLSession
    
    
    init(urlSession: URLSession = URLSession(configuration: .ephemeral)) {
        self.urlSession = urlSession
    }
    
    
    func fetchData(url: URL) async throws -> Data {
        let (data,response) = try await urlSession.data(from: url)
        guard let httURLResponse = response as? HTTPURLResponse,httURLResponse.statusCode == 200 else {
            throw URLError(.badServerResponse)
        }
        
        
        return data
    }
    
    
}
