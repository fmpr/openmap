import Foundation
import SwiftUI

// Model for weather
// Dummy:  let weatherModel = try? JSONDecoder().decode(weatherModel.self, from: jsonData)




// we use optional because sometimes our model is not complete when fetching information

// MARK: - weatherModel
struct weatherModel: Codable {
    var coord: Coord?
    var weather: [Weather]?
    var base: String?
    var main: Main?
    var visibility: Int?
    var wind: Wind?
    var rain: Rain?
    var clouds: Clouds?
    var dt: Int?
    var sys: Sys?
    var timezone:Int?
    var id: Int?
    var name: String?
    var cod: Int?
}

// MARK: - Coord
struct Coord: Codable {
    var lon, lat: Double?
}

// MARK: - Weather
struct Weather: Codable {
    var id: Int
    var main, description, icon: String
}

// MARK: - Main
struct Main: Codable {
    var temp, feels_like, temp_min, temp_max: Double?
    var pressure, humidity: Int?

    /*
    enum CodingKeys: String, CodingKey {
        case temp
        case feels_like = "feels_like"
        case temp_min = "temp_min"
        case temp_max = "temp_max"
        case pressure, humidity
    }
    */
}

// MARK: - Rain
struct Rain: Codable {
    var the1H: Double?

    enum CodingKeys: String, CodingKey {
        case the1H = "1h"
    }
     
}
// MARK: - Clouds
struct Clouds: Codable {
    var all: Int?
}

// MARK: - Sys
struct Sys: Codable {
    var type, id: Int?
    var country: String?
    var sunrise, sunset: Int?
}


// MARK: - Wind
struct Wind: Codable {
    var speed: Double?
    var deg: Int?
}

