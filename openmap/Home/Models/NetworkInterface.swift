//
//  NetworkProtocol.swift
//  openmap
//
//  Created by Francisco Perez on 22/05/23.
//

import Foundation


protocol NetworkInterface {
    func fetchData(url:URL) async throws -> Data
}
