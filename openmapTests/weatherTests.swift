//
//  weatherTests.swift
//  openmapTests
//
//  Created by Francisco Perez on 22/05/23.
//

import XCTest
@testable import openmap

final class weatherTests: XCTestCase {

    //let app = XCUIApplication()
    var sut : weatherVM = weatherVM()
    
    
    
    // i validate the URL
    func testValidateOpenMapURL() {
     
     let exp  = expectation(description: "Download openmap data")
     let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=Laredo,US&appid=97fb0baad55faa308a3ff5ccc0a2d19e")!
     
        URLSession.shared.dataTask(with: url) { data, _, _ in
            
            XCTAssertNotNil(data,"No data downloaded")
            exp.fulfill()
        }.resume()
        
        wait(for: [exp],timeout: 10.1)
    }
    
    
    
    
    // when failed we throw an error from the view model
    // throw URLError(.badURL)
    
    
    func testGetAPIDataFromCity() async throws {
        do {
            try await sut.getAPIData(city: "Princeton")
            XCTAssert(sut.isFetchCalled)
        }catch{
            print("error_fromgetAPI")
            XCTAssertThrowsError("error_fromgetAPI URLError(.badURL)")
        }
    }
    
    
} // end
